// @ts-nocheck
export var orderEntities = function (entityA, entityB) {
    if (!entityA || !entityA.pos) {
        return -1;
    }
    if (!entityB || !entityB.pos) {
        return 1;
    }
    var numA = parseFloat(entityA.pos);
    var numB = parseFloat(entityB.pos);
    return precisionRound(numA) - precisionRound(numB);
};
export var precisionRound = function (numberToCompute, precision) {
    if (precision === void 0) { precision = 100; }
    var factor = Math.pow(10, precision);
    return Math.round(numberToCompute * factor) / factor;
};
export var computeCustomPropertyName = function (token, categories) {
    if (token.categoryId) {
        var categoryName = categories[token.categoryId].name
            .toLowerCase()
            .replace(/ /g, "");
        var tokenName = token.name.toLowerCase().replace(/ /g, "");
        return "--dl-".concat(token.type, "-").concat(categoryName, "-").concat(tokenName);
    }
    else {
        return "--dl-".concat(token.type, "-").concat(token.name.toLowerCase());
    }
};
//# sourceMappingURL=utils.js.map