"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getResetStylesheet = exports.getProjectGlobalStylesheet = exports.DEFAULT_THEME_CONSTANTS = void 0;
exports.DEFAULT_THEME_CONSTANTS = {
    primaryColor: "#0089be",
    backgroundColor: "#fff",
    borderRadius: "4px",
    borderWidth: "1px",
    fontSize: "16px",
    color: "#2b2b2b",
    lineHeight: "1.55",
    fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen","Ubuntu", "Cantarell", "Fira Sans","Droid Sans", "Helvetica Neue", sans-serif',
    fontStyle: "normal",
    letterSpacing: "normal",
};
var getProjectGlobalStylesheet = function (stylesFromTokens, targetStage) {
    var _a, _b, _c, _d, _e, _f, _g;
    return "\n    body {\n      font-family: ".concat((_a = stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.fontFamily) !== null && _a !== void 0 ? _a : exports.DEFAULT_THEME_CONSTANTS.fontFamily, ";\n      font-size: ").concat((_b = stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.fontSize) !== null && _b !== void 0 ? _b : exports.DEFAULT_THEME_CONSTANTS.fontSize, ";\n      font-weight: ").concat(stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.fontWeight, ";\n      font-style:").concat((_c = stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.fontStyle) !== null && _c !== void 0 ? _c : exports.DEFAULT_THEME_CONSTANTS.fontStyle, ";\n      text-decoration: ").concat(stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.textDecoration, ";\n      text-transform: ").concat(stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.textTransform, ";\n      letter-spacing: ").concat((_d = stylesFromTokens === null || stylesFromTokens === void 0 ? void 0 : stylesFromTokens.letterSpacing) !== null && _d !== void 0 ? _d : exports.DEFAULT_THEME_CONSTANTS.letterSpacing, ";\n      color: ").concat((_e = stylesFromTokens.color) !== null && _e !== void 0 ? _e : exports.DEFAULT_THEME_CONSTANTS.color, ";\n      background-color: ").concat((_f = stylesFromTokens.background) !== null && _f !== void 0 ? _f : exports.DEFAULT_THEME_CONSTANTS.backgroundColor, ";\n      line-height: ").concat((_g = stylesFromTokens.lineHeight) !== null && _g !== void 0 ? _g : exports.DEFAULT_THEME_CONSTANTS.lineHeight, ";\n      ").concat(targetStage ? "user-select: none;" : "", "\n    }\n  \n    ").concat(targetStage ? "*:focus { \n outline: none; \n }" : "", "\n  ");
};
exports.getProjectGlobalStylesheet = getProjectGlobalStylesheet;
var getResetStylesheet = function () { return "\nhtml {\n  line-height: 1.15;\n}\nbody {\n  margin: 0;\n}\n\n* {\n  box-sizing: border-box;\n  border-width: 0;\n  border-style: solid;\n}\n\np,\nli,\nul,\npre,\ndiv,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  margin: 0;\n  padding: 0;\n}\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit;\n  font-size: 100%;\n  line-height: 1.15;\n  margin: 0;\n}\n\nbutton,\nselect {\n  text-transform: none;\n}\n\nbutton,\n[type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n}\n\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0;\n}\n\nbutton:-moz-focus,\n[type=\"button\"]:-moz-focus,\n[type=\"reset\"]:-moz-focus,\n[type=\"submit\"]:-moz-focus {\n  outline: 1px dotted ButtonText;\n}\n\na {\n  color: inherit;\n  text-decoration: inherit;\n}\n\ninput {\n  padding: 2px 4px;\n}\n\nimg {\n  display: block;\n}\n"; };
exports.getResetStylesheet = getResetStylesheet;
//# sourceMappingURL=constants.js.map