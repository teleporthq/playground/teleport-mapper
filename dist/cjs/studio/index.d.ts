import { UIDLGlobalProjectValues, UIDLPropDefinition, UIDLStateValueDetails, VComponentUIDL, VProjectUIDL, VUIDLElementNode, VUIDLStyleSetDefnition, UIDLLinkNode } from "@teleporthq/teleport-types";
import { ProjectSnapshot } from "./types";
export interface MapSnapshowToUIDLInterface {
    generatePageMetaData: (pageId: string) => UIDLStateValueDetails;
    nodeToUIDL: (nodeId: string, compId: string, parentNode?: VUIDLElementNode) => VUIDLElementNode | null;
    stylesToUIDL: (nodeId: string) => Record<string, VUIDLStyleSetDefnition>;
    componentToUIDL: (compId: string) => VComponentUIDL | null;
    pageToUIDL: (pageId: string) => VComponentUIDL | null;
    toProjectUIDL: (snapshot: Record<string, unknown>) => VProjectUIDL;
}
export declare class MapSnapshotToUIDL implements MapSnapshowToUIDLInterface {
    snapshot: ProjectSnapshot;
    constructor(snapshot: Record<string, unknown>);
    getDefaultStylesFromTokens(): {
        background: string;
        color: string;
    };
    generatePageMetaData(pageId: string): UIDLStateValueDetails | {
        value: any;
    };
    propsToUIDL(props: Record<string, unknown>): Record<string, UIDLPropDefinition>;
    attrsToUIDL(attrs: Record<string, unknown>, compId: string): string;
    stylesToUIDL(nodeId: string): Record<string, VUIDLStyleSetDefnition>;
    abilitiesToUIDL(abilities: Record<string, unknown>, compId: string): {
        link: UIDLLinkNode;
    };
    nodeToUIDL(nodeId: string, compId: string, parentNode?: VUIDLElementNode): VUIDLElementNode | null;
    componentToUIDL(compId: string): VComponentUIDL | null;
    pageToUIDL(pageId: string): VComponentUIDL;
    styleSetDefinitionsToUIDL(): Record<string, VUIDLStyleSetDefnition>;
    toProjectUIDL(): VProjectUIDL;
    projectSettingsToUIDL(): UIDLGlobalProjectValues;
    private getTokenWithRole;
    private getTokensAsCustomProperties;
    private getDefaultTextStyle;
}
