"use strict";
// @ts-nocheck
Object.defineProperty(exports, "__esModule", { value: true });
exports.computeCustomPropertyName = exports.precisionRound = exports.orderEntities = void 0;
var orderEntities = function (entityA, entityB) {
    if (!entityA || !entityA.pos) {
        return -1;
    }
    if (!entityB || !entityB.pos) {
        return 1;
    }
    var numA = parseFloat(entityA.pos);
    var numB = parseFloat(entityB.pos);
    return (0, exports.precisionRound)(numA) - (0, exports.precisionRound)(numB);
};
exports.orderEntities = orderEntities;
var precisionRound = function (numberToCompute, precision) {
    if (precision === void 0) { precision = 100; }
    var factor = Math.pow(10, precision);
    return Math.round(numberToCompute * factor) / factor;
};
exports.precisionRound = precisionRound;
var computeCustomPropertyName = function (token, categories) {
    if (token.categoryId) {
        var categoryName = categories[token.categoryId].name
            .toLowerCase()
            .replace(/ /g, "");
        var tokenName = token.name.toLowerCase().replace(/ /g, "");
        return "--dl-".concat(token.type, "-").concat(categoryName, "-").concat(tokenName);
    }
    else {
        return "--dl-".concat(token.type, "-").concat(token.name.toLowerCase());
    }
};
exports.computeCustomPropertyName = computeCustomPropertyName;
//# sourceMappingURL=utils.js.map