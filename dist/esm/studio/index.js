var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { orderEntities, computeCustomPropertyName } from "./utils";
import { getResetStylesheet, getProjectGlobalStylesheet } from "./constants";
var MapSnapshotToUIDL = /** @class */ (function () {
    function MapSnapshotToUIDL(snapshot) {
        this.snapshot = snapshot;
    }
    MapSnapshotToUIDL.prototype.getDefaultStylesFromTokens = function () {
        var _a, _b;
        var defaultBackgroundId = (_a = this.getTokenWithRole("background-color")) === null || _a === void 0 ? void 0 : _a.id;
        var defaultForegroundId = (_b = this.getTokenWithRole("foreground-color")) === null || _b === void 0 ? void 0 : _b.id;
        var variablesMapping = this.getTokensAsCustomProperties();
        var textStyle = this.getDefaultTextStyle();
        var content = textStyle.content;
        return __assign(__assign(__assign({}, (defaultForegroundId
            ? { color: "var(".concat(variablesMapping[defaultForegroundId].name, ")") }
            : null)), (defaultBackgroundId
            ? { background: "var(".concat(variablesMapping[defaultBackgroundId].name, ")") }
            : null)), Object.keys(content || {}).reduce(function (acc, key) {
            var _a;
            if (((_a = content[key]) === null || _a === void 0 ? void 0 : _a.type) === "static") {
                acc[key] = content[key].content;
            }
            return acc;
        }, {}));
    };
    MapSnapshotToUIDL.prototype.generatePageMetaData = function (pageId) {
        var _a, _b, _c, _d, _e, _f, _g;
        var pagesById = this.snapshot.pages.byId;
        var assetsById = this.snapshot.assets.byId;
        var page = pagesById[pageId];
        var pageSettings = page === null || page === void 0 ? void 0 : page.settings;
        var projectSettings = ((_a = this.snapshot) === null || _a === void 0 ? void 0 : _a.settings) || {};
        if (!page) {
            throw new Error("Page ".concat(pageId, " could not be found in project ").concat(this.snapshot.title));
        }
        if (!pageSettings) {
            return { value: page.title };
        }
        var seoTitleFromProject = ((_b = projectSettings.general) === null || _b === void 0 ? void 0 : _b.title) || this.snapshot.title;
        var titlePlaceholder = (page === null || page === void 0 ? void 0 : page.isHomePage)
            ? seoTitleFromProject
            : page.title + " - " + seoTitleFromProject;
        var projectImageAsset = ((_c = projectSettings.social) === null || _c === void 0 ? void 0 : _c.image)
            ? assetsById[(_d = projectSettings.social) === null || _d === void 0 ? void 0 : _d.image]
            : undefined;
        var pageImageAsset = pageSettings.socialImage
            ? assetsById[pageSettings.socialImage]
            : undefined;
        var metaTags = [
            {
                name: "description",
                content: pageSettings.seoDescription || ((_e = projectSettings.general) === null || _e === void 0 ? void 0 : _e.description),
            },
            {
                property: "og:title",
                content: pageSettings.socialTitle || pageSettings.seoTitle || titlePlaceholder,
            },
            {
                property: "og:description",
                content: pageSettings.socialDescription ||
                    ((_f = projectSettings.social) === null || _f === void 0 ? void 0 : _f.description) ||
                    pageSettings.seoDescription ||
                    ((_g = projectSettings.general) === null || _g === void 0 ? void 0 : _g.description),
            },
            {
                property: "og:image",
                content: (pageImageAsset === null || pageImageAsset === void 0 ? void 0 : pageImageAsset.remoteSrc) || (projectImageAsset === null || projectImageAsset === void 0 ? void 0 : projectImageAsset.remoteSrc),
            },
        ];
        var filteredMetaTags = metaTags.filter(function (tag) { return tag.content; });
        var seo = {
            title: pageSettings.seoTitle || titlePlaceholder,
            metaTags: filteredMetaTags,
        };
        var stateDefinitions = { value: page.title, seo: seo };
        Object.assign(stateDefinitions, pageSettings.url ? { pageOptions: { navLink: pageSettings.url } } : {});
        return stateDefinitions;
    };
    MapSnapshotToUIDL.prototype.propsToUIDL = function (props) {
        if (!props) {
            return null;
        }
        return Object.values(props).reduce(function (acc, prop) {
            acc[prop.name] = {
                type: prop.type,
                defaultValue: prop.defaultValue,
            };
            return acc;
        }, {});
    };
    MapSnapshotToUIDL.prototype.attrsToUIDL = function (attrs, compId) {
        var _a;
        if (!attrs) {
            return null;
        }
        var componentsById = this.snapshot.components.byId;
        var pagesById = this.snapshot.pages.byId;
        var compProps = (_a = (componentsById[compId] || pagesById[compId])) === null || _a === void 0 ? void 0 : _a.propDefinitions;
        return Object.keys(attrs).reduce(function (acc, attrId) {
            var attr = attrs[attrId];
            if (attr.type === "static") {
                acc[attrId] = {
                    type: "static",
                    content: String(attr.content),
                };
            }
            if (attr.type === "dynamic" && attr.content.referenceType === "prop") {
                var usedProp = compProps[attr.content.id];
                if (usedProp) {
                    acc[attrId] = {
                        type: "dynamic",
                        content: {
                            referenceType: "prop",
                            id: usedProp.name,
                        },
                    };
                }
            }
            return acc;
        }, {});
    };
    MapSnapshotToUIDL.prototype.stylesToUIDL = function (nodeId) {
        var _a;
        var styles = ((_a = this.snapshot.nodes.byId[nodeId]) === null || _a === void 0 ? void 0 : _a.styles) || {};
        var _b = this.snapshot.designLanguage, _c = _b.tokensById, tokensById = _c === void 0 ? {} : _c, _d = _b.categoriesById, categoriesById = _d === void 0 ? {} : _d;
        return Object.keys(styles || {}).reduce(function (acc, styleKey) {
            var _a;
            var style = styles[styleKey];
            if (style.type === "static" && (style === null || style === void 0 ? void 0 : style.content)) {
                acc[styleKey] = {
                    type: "static",
                    content: style.content,
                };
            }
            if (style.type === "dynamic" &&
                ((_a = style.content) === null || _a === void 0 ? void 0 : _a.referenceType) === "token") {
                var usedToken = tokensById[style.content.id];
                acc[styleKey] = {
                    type: "dynamic",
                    content: {
                        referenceType: "token",
                        id: computeCustomPropertyName(usedToken, categoriesById),
                    },
                };
            }
            return acc;
        }, {});
    };
    MapSnapshotToUIDL.prototype.abilitiesToUIDL = function (abilities, compId) {
        var _a;
        if (!((_a = abilities.link) === null || _a === void 0 ? void 0 : _a.type)) {
            return {};
        }
        switch (abilities.link.type) {
            case "section":
            case "phone":
            case "mail":
            case "url": {
                return { link: abilities.link };
            }
            case "navlink": {
                var pageId = abilities.link.content.routeName;
                var page = this.snapshot.pages.byId[pageId];
                if (!page) {
                    return {};
                }
                return {
                    link: {
                        type: "navlink",
                        content: {
                            routeName: (page === null || page === void 0 ? void 0 : page.title) || (page === null || page === void 0 ? void 0 : page.id),
                        },
                    },
                };
            }
            default: {
                return {};
            }
        }
    };
    MapSnapshotToUIDL.prototype.nodeToUIDL = function (nodeId, compId, parentNode) {
        var _this = this;
        var nodesById = this.snapshot.nodes.byId;
        var componentsById = this.snapshot.components.byId;
        var pagesById = this.snapshot.pages.byId;
        var node = nodesById[nodeId];
        if (!node) {
            return null;
        }
        var primitiveType = node.primitiveType, _a = node.attrs, attrs = _a === void 0 ? {} : _a, semanticType = node.semanticType, _b = node.childrenIds, childrenIds = _b === void 0 ? {} : _b, _c = node.contentChildren, contentChildren = _c === void 0 ? {} : _c, compIdOfCompInstance = node.compIdOfCompInstance, _d = node.abilities, abilities = _d === void 0 ? {} : _d;
        var style = this.stylesToUIDL(node.id);
        var elementNode = {
            type: "element",
            content: __assign(__assign(__assign(__assign(__assign({ elementType: primitiveType }, (semanticType && { semanticType: semanticType })), (Object.keys(attrs || {}).length > 0 && {
                attrs: this.attrsToUIDL(attrs, compId),
            })), (Object.keys(style).length > 0 && { style: style })), ((abilities === null || abilities === void 0 ? void 0 : abilities.link) && {
                abilities: this.abilitiesToUIDL(abilities, compId),
            })), { children: [] }),
        };
        if (primitiveType === "component" && compIdOfCompInstance) {
            var usedComponent = componentsById[compIdOfCompInstance];
            delete elementNode.content.children;
            elementNode = __assign(__assign({}, elementNode), { content: __assign(__assign({}, elementNode.content), { elementType: primitiveType, semanticType: usedComponent.title, dependency: {
                        type: "local",
                    } }) });
            if ((parentNode === null || parentNode === void 0 ? void 0 : parentNode.type) === "element") {
                parentNode.content.children.push(elementNode);
            }
            return elementNode;
        }
        Object.values(contentChildren).forEach(function (contentNode) {
            var _a;
            if (contentNode.type === "static" && (contentNode === null || contentNode === void 0 ? void 0 : contentNode.content)) {
                elementNode.content.children.push({
                    type: "static",
                    content: contentNode.content,
                });
            }
            if (contentNode.type === "dynamic" &&
                contentNode.content.referenceType === "prop") {
                var usedProp = (_a = (componentsById[compId] || pagesById[compId])) === null || _a === void 0 ? void 0 : _a.propDefinitions[contentNode.content.id];
                if (!usedProp) {
                    return;
                }
                elementNode.content.children.push({
                    type: "dynamic",
                    content: {
                        referenceType: "prop",
                        id: usedProp.name,
                    },
                });
            }
        });
        Object.keys(childrenIds)
            .map(function (childId) { return nodesById[childId]; })
            .sort(orderEntities)
            .forEach(function (childNode) {
            return _this.nodeToUIDL(childNode.id, compId, elementNode);
        });
        if ((parentNode === null || parentNode === void 0 ? void 0 : parentNode.type) === "element") {
            parentNode.content.children.push(elementNode);
        }
        return elementNode;
    };
    MapSnapshotToUIDL.prototype.componentToUIDL = function (compId) {
        var componentsById = this.snapshot.components.byId;
        var comp = componentsById[compId];
        if (!comp) {
            throw new Error("Component missing from the project");
        }
        var title = comp.title, rootNodeId = comp.rootNodeId, compProps = comp.propDefinitions;
        var propDefinitions = this.propsToUIDL(compProps);
        var component = {
            name: title,
            propDefinitions: propDefinitions,
            node: this.nodeToUIDL(rootNodeId, compId),
        };
        return component;
    };
    MapSnapshotToUIDL.prototype.pageToUIDL = function (pageId) {
        if (this.snapshot.components.byId[pageId]) {
            return this.componentToUIDL(pageId);
        }
        var usedPage = this.snapshot.pages.byId[pageId];
        if (!usedPage) {
            return;
        }
        var title = usedPage.title, rootNodeId = usedPage.rootNodeId;
        var page = {
            name: title,
            node: this.nodeToUIDL(rootNodeId, pageId),
        };
        return page;
    };
    MapSnapshotToUIDL.prototype.styleSetDefinitionsToUIDL = function () {
        var designLanguage = this.snapshot.designLanguage;
        var _a = designLanguage.textStyleSetsById, textStyleSetsById = _a === void 0 ? {} : _a;
        return Object.values(textStyleSetsById).reduce(function (acc, styleRef) {
            acc[styleRef.id] = {
                id: styleRef.id,
                name: styleRef.name,
                type: "reusable-project-style-map",
                content: Object.keys(styleRef.content).reduce(function (styleAcc, styleId) {
                    var style = styleRef.content[styleId];
                    if (style.type === "static") {
                        styleAcc[styleId] = {
                            type: "static",
                            content: style.content,
                        };
                    }
                    return styleAcc;
                }, {}),
            };
            return acc;
        }, {});
    };
    MapSnapshotToUIDL.prototype.toProjectUIDL = function () {
        var _this = this;
        var _a;
        var _b = this.snapshot, components = _b.components, pages = _b.pages;
        var componentsById = components.byId;
        var pagesById = pages.byId;
        var uidl = {};
        var homePage = Object.values(pagesById).find(function (page) { return page.isHomePage; });
        var tokens = Object.values(this.getTokensAsCustomProperties() || {}).reduce(function (acc, tokenRef) {
            acc[tokenRef.name] = {
                type: "static",
                content: tokenRef.value,
            };
            return acc;
        }, {});
        var uidlPages = Object.values(pagesById).map(function (page) {
            var pageUIDL = _this.pageToUIDL(page.id);
            pageUIDL.node.content.style.minHeight = {
                type: "static",
                content: "100vh",
            };
            return {
                type: "conditional",
                content: {
                    node: pageUIDL.node,
                    value: page.title,
                    reference: {
                        type: "dynamic",
                        content: {
                            referenceType: "state",
                            id: "route",
                        },
                    },
                },
            };
        });
        uidl = {
            globals: this.projectSettingsToUIDL(),
            name: this.snapshot.title,
            root: {
                name: "App",
                styleSetDefinitions: this.styleSetDefinitionsToUIDL(),
                designLanguage: {
                    tokens: tokens,
                },
                stateDefinitions: {
                    route: {
                        type: "string",
                        defaultValue: (_a = homePage === null || homePage === void 0 ? void 0 : homePage.title) !== null && _a !== void 0 ? _a : "index",
                        values: Object.keys(pagesById).map(function (pageId) {
                            return _this.generatePageMetaData(pageId);
                        }),
                    },
                },
                node: {
                    type: "element",
                    content: {
                        elementType: "Router",
                        children: uidlPages,
                    },
                },
            },
        };
        Object.values(componentsById || {}).forEach(function (comp) {
            var _a;
            var component = _this.componentToUIDL(comp.id);
            if (component) {
                uidl = __assign(__assign({}, uidl), { components: __assign(__assign({}, uidl.components), (_a = {}, _a[comp.title] = component, _a)) });
            }
        });
        return uidl;
    };
    MapSnapshotToUIDL.prototype.projectSettingsToUIDL = function () {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
        var _o = this.snapshot, title = _o.title, settings = _o.settings;
        var fontsById = this.snapshot.fonts.byId;
        var stylesFromTokens = this.getDefaultStylesFromTokens();
        var globals = {
            settings: {
                title: ((_a = settings.general) === null || _a === void 0 ? void 0 : _a.title) || title,
                language: ((_b = settings.general) === null || _b === void 0 ? void 0 : _b.language) || "en",
            },
            assets: [
                {
                    type: "style",
                    content: getResetStylesheet().split("\n").join(""),
                },
                {
                    type: "style",
                    content: getProjectGlobalStylesheet(stylesFromTokens),
                },
            ],
            meta: [
                {
                    name: "viewport",
                    content: "width=device-width, initial-scale=1.0",
                },
                {
                    charSet: "utf-8",
                },
                {
                    property: "twitter:card",
                    content: "summary_large_image",
                },
            ],
        };
        if (!settings) {
            return globals;
        }
        // ASSETS
        // favicon
        if ((_c = settings.general) === null || _c === void 0 ? void 0 : _c.favicon) {
            var faviconAsset = this.snapshot.assets.byId[(_d = settings.general) === null || _d === void 0 ? void 0 : _d.favicon];
            if (faviconAsset) {
                (_e = globals.assets) === null || _e === void 0 ? void 0 : _e.push({
                    type: "icon",
                    path: faviconAsset.imageSrc,
                    options: { iconType: "icon/png", iconSizes: "32x32" },
                });
            }
        }
        // fonts
        Object.values(fontsById || {}).forEach(function (font) {
            var _a;
            (_a = globals.assets) === null || _a === void 0 ? void 0 : _a.push({
                type: "font",
                path: font.path,
            });
        });
        // Google scripts
        if ((_f = settings.code) === null || _f === void 0 ? void 0 : _f.googleAnalyticsId) {
            (_g = globals.assets) === null || _g === void 0 ? void 0 : _g.push({
                type: "script",
                path: "https://www.googletagmanager.com/gtag/js?id=".concat(settings.code.googleAnalyticsId),
                options: {
                    target: "body",
                    async: true,
                },
            });
            (_h = globals.assets) === null || _h === void 0 ? void 0 : _h.push({
                type: "script",
                content: "window.dataLayer = window.dataLayer || [];\n          function gtag(){dataLayer.push(arguments);}\n          gtag('js', new Date());\n          gtag('config', '".concat(settings.code.googleAnalyticsId, "');"),
                options: {
                    target: "body",
                },
            });
        }
        if ((_j = settings.code) === null || _j === void 0 ? void 0 : _j.tagManagerId) {
            (_k = globals.assets) === null || _k === void 0 ? void 0 : _k.push({
                type: "script",
                content: "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':\n          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],\n          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=\n          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);\n          })(window,document,'script','dataLayer','".concat(settings.code.tagManagerId, "');"),
                options: {
                    target: "body",
                },
            });
        }
        globals.customCode = {};
        if ((_l = settings.code) === null || _l === void 0 ? void 0 : _l.headTag) {
            globals.customCode.head = settings.code.headTag;
        }
        if ((_m = settings.code) === null || _m === void 0 ? void 0 : _m.bodyTag) {
            globals.customCode.body = settings.code.bodyTag;
        }
        return globals;
    };
    MapSnapshotToUIDL.prototype.getTokenWithRole = function (role) {
        var designLanguage = this.snapshot.designLanguage;
        return Object.values((designLanguage === null || designLanguage === void 0 ? void 0 : designLanguage.tokensById) || {}).find(function (token) { return token.role === role; });
    };
    MapSnapshotToUIDL.prototype.getTokensAsCustomProperties = function () {
        var _a = this.snapshot.designLanguage, _b = _a.tokensById, tokensById = _b === void 0 ? {} : _b, _c = _a.categoriesById, categoriesById = _c === void 0 ? {} : _c;
        return Object.keys(tokensById || {}).reduce(function (acc, tokenId) {
            acc[tokenId] = {
                name: computeCustomPropertyName(tokensById[tokenId], categoriesById),
                value: tokensById[tokenId].value,
            };
            return acc;
        }, {});
    };
    MapSnapshotToUIDL.prototype.getDefaultTextStyle = function () {
        var _a = this.snapshot.designLanguage.textStyleSetsById, textStyleSetsById = _a === void 0 ? {} : _a;
        return Object.values(textStyleSetsById).find(function (textStyle) { return (textStyle === null || textStyle === void 0 ? void 0 : textStyle.role) === "default"; });
    };
    return MapSnapshotToUIDL;
}());
export { MapSnapshotToUIDL };
//# sourceMappingURL=index.js.map