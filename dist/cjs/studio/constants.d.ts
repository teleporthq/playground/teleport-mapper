export declare const DEFAULT_THEME_CONSTANTS: {
    primaryColor: string;
    backgroundColor: string;
    borderRadius: string;
    borderWidth: string;
    fontSize: string;
    color: string;
    lineHeight: string;
    fontFamily: string;
    fontStyle: string;
    letterSpacing: string;
};
export declare const getProjectGlobalStylesheet: (stylesFromTokens: Record<string, string>, targetStage?: boolean) => string;
export declare const getResetStylesheet: () => string;
