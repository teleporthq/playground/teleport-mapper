export declare const orderEntities: (entityA: Record<string, string>, entityB: Record<string, string>) => number;
export declare const precisionRound: (numberToCompute: number, precision?: number) => number;
export declare const computeCustomPropertyName: (token: unknown, categories: Record<string, unknown>) => string;
